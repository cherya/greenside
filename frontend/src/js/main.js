/*
 * Third party
 */
//= ../../bower_components/jquery/dist/jquery.js
//= ../../bower_components/owl.carousel/dist/owl.carousel.min.js
//= ../../bower_components/lightslider/dist/js/lightslider.min.js

function initGallery() {
   var slider = false;
   var galleryModal = $('.gallery-modal');
   var sliderInst;

   function initSlider() {
      sliderInst = $("#light-slider").lightSlider({
         item: 1,
         thumbItem: 5,
         slideMargin: 100,
         loop: true,
         controls: true
      });
   }

   galleryModal.on('click', function(event) {
      if (!$(event.target).parents('.light-slider-wrapper').length) {
         $('.gallery-modal').addClass('hide');
         $('body').removeClass('no-scroll');
         sliderInst.goToSlide(1);
      }
   });

   $('.gallery_item').on('click', function() {
      $('.gallery-modal').removeClass('hide');
      $('body').addClass('no-scroll');
      if (!slider) {
         slider = true;
         initSlider();
      }
   });
}

function initMenu() {
   var menuModal = $('.menu-modal');
   $('.menu_card').on('click', function() {
      setTimeout(function() {
         menuModal.css('opacity', 1)
      }, 0);
      menuModal.removeClass('hide');
      $('body').addClass('no-scroll');
   });

   $('.close', menuModal).on('click', function(){
      menuModal.css('opacity', 0);
      $('body').removeClass('no-scroll');
      setTimeout(function() {
         menuModal.addClass('hide');
      }, 500);
   });

   var pageTitles = $('.page-title');
   // animate current marker and page slide
   pageTitles.on('click', function() {
      var $this = $(this);
      var index = $this.index();
      var wrapper = $('.pages-wrapper');
      pageTitles.removeClass('current');
      $this.addClass('current');
      $('.cursor').css('transform', 'translate(' + ($this.position().left) + 'px, ' + $this.position().top + 'px)');
      if (index === 0) {
         wrapper.css('transform', 'translateX(0)');
      } else {
         wrapper.css('transform', 'translateX(-' + 100 * index + 'vw)')
      }
   });
}

function initEnteringWindow() {
   var enterModal = $('.enter-modal');
   $('.yep', enterModal).on('click', function(event) {
      enterModal.addClass('invisible');
      $('body').removeClass('no-scroll');
      
      $('section').each(function(index, el) {
         var $window = $(window);
         var element = $(this);
         var elBottom = element.offset().top + element.height();
         if ($window.scrollTop() + $window.height() > elBottom + 50) {
            element.next('section').addClass('scrolled');
         }
      });

      setTimeout(function() {
         enterModal.hide();
      }, 500);
   });
}

jQuery(document).ready(function($) {

   initGallery();
   initEnteringWindow();
   initMenu();

   // another shit
   $('body').on('click', '.side-toggle', function(event) {
      $('body').toggleClass('side-open');
   });

   $('body').on('click', '.page .body', function(event) {
      $('body').removeClass('side-open');
   });

   var sections = $('.page > .body > section');
   $(window).scroll(function(event) {
      sections.each(function(index, el) {
         var element = $(this);
         var elTop = element.offset().top;
         var elBottom = elTop + element.height();
         var $window = $(window);
         if ($window.scrollTop() + $window.height() > elBottom + 50) {
            element.next('section').addClass('scrolled');
         }
         if ($window.scrollTop() > elTop - 50 && $window.scrollTop() < elBottom) {
            var id = element.attr('id');
            $('.page > .side> .nav li').removeClass('active');
            $('.page > .side> .nav a[href="#' + id + '"]').parent('li').addClass('active');
         }
      });
   });

   $('a[href^="#"]').bind('click.smoothscroll', function(e) {
      e.preventDefault();

      var target = this.hash,
         $target = $(target);

      $('html, body').stop().animate({
         'scrollTop': $(this.hash).offset().top
      }, 500, 'swing', function() {
         window.location.hash = target;
         setTimeout(function() {
            $('body').removeClass('side-open');
         }, 300);
      });
   });

   $('.reviews.owl-carousel').owlCarousel({
      margin: 10,
      autoWidth: true,
      items: 4,
      nav: true,
      // autoplay: true
   });

   $('.about.owl-carousel').owlCarousel({
      margin: 50,
      autoWidth: true,
      items: 2,
      nav: true,
      // loop: true,
      videoHeight: true
         // autoplay: true
   });
});