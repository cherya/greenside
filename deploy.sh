cd frontend
gulp build
cd ..

echo "clean..."
rm -rf greenside/assets/
rm -rf greenside/_generated/assets/
echo "copy assets ..."
cp -R frontend/build/. greenside/assets/
echo "copy _generated/assets ..."
cp -R frontend/build/. greenside/_generated/assets/
echo "copy admin_extensions ..."
cp -R frontend/src/admin_extensions/. greenside/assets/admin_extensions/
echo "running server ..."
cd greenside
enduro start
echo "done"