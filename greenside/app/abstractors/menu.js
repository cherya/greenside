// placeholder abstractor

const abstractor = function () {}

const classesRotate = ['sheet-right-top', 
                       'sheet-left-bottom', 
                       'sheet-left-bottom-rotate', 
                       'sheet-right-top', 
                       'sheet-left-bottom',
                       'sheet-right-top',
                       'sheet-left-bottom',
                       'sheet-left-bottom-rotate']

abstractor.prototype.init = function (context) {
   return new Promise(function (resolve, reject) {

      // initialize abstractor
      resolve()
   })
}

abstractor.prototype.abstract = function (context) {
   return new Promise(function (resolve, reject) {
      menu = context.menu;
      for (let i = 0; i < menu.length; i++) {
         let cMenu = menu[i];
         if (cMenu.table) {
            for (let j = 0; j < cMenu.blocks.length; j++) {
               for (let k = 0; k < cMenu.blocks[j].items.length; k++) {
                  let item = cMenu.blocks[j].items[k];
                  if (!item.price) {
                     item.isTitle = true;
                  }
               }
            }
         }
      }
      return resolve()
   })
}

module.exports = new abstractor()
