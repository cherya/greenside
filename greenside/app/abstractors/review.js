//	placeholder abstractor

const abstractor = function () {}

const classesRotate = ['sheet-right-top', 
                       'sheet-left-bottom', 
                       'sheet-left-bottom-rotate', 
                       'sheet-right-top', 
                       'sheet-left-bottom',
                       'sheet-right-top',
                       'sheet-left-bottom',
                       'sheet-left-bottom-rotate']

abstractor.prototype.init = function (context) {
	return new Promise(function (resolve, reject) {

		// initialize abstractor
		resolve()
	})
}

abstractor.prototype.abstract = function (context) {
	return new Promise(function (resolve, reject) {
      reviews = context.review;
      parsed_reviews = []
      for (let i = 0; i < reviews.length; i++) {
         const review = Object.assign({}, reviews[i]);
         review.class = classesRotate[i % classesRotate.length];
         parsed_reviews.push(review);
      }
      context['$parsed_reviews_hidden'] = true;
      context['$parsed_reviews_hidden'] = true;
      context.parsed_reviews = parsed_reviews;
		return resolve()
	})
}

module.exports = new abstractor()
