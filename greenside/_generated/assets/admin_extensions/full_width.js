var pages = ['#!/cms/global/seo', '#!/cms/global/menu'];

function setFullWidth() {
   if (pages.indexOf(window.location.hash) !== -1) {
      $('.col-md-4').removeClass('col-md-4').addClass('col-md-12');
   }
}

setInterval(setFullWidth, 500);