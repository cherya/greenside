{
   $title_type: 'title',
   title: 'Фото',
   photos: [
      {
         $photo_type: 'image',
         photo: '/assets/img/gallery-photo-1.png',
         main: true
      },
      {
         $photo_type: 'image',
         photo: '/assets/img/gallery-photo-2.png',
         main: true
      },
      {
         $photo_type: 'image',
         photo: '/assets/img/gallery-photo-3.png',
         main: true
      },
      {
         $photo_type: 'image',
         photo: '/assets/img/gallery-photo-1.png',
         main: false
      },
      {
         $photo_type: 'image',
         photo: '/assets/img/gallery-photo-2.png',
         main: false
      },
      {
         $photo_type: 'image',
         photo: '/assets/img/gallery-photo-3.png',
         main: false
      },
      {
         $photo_type: 'image',
         photo: '/assets/img/gallery-photo-1.png',
         main: false
      },
      {
         $photo_type: 'image',
         photo: '/assets/img/gallery-photo-2.png',
         main: false
      },
      {
         $photo_type: 'image',
         photo: '/assets/img/gallery-photo-3.png',
         main: false
      }
   ]
}