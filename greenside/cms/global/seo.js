{
	$title_type: 'title',
	title: 'Все теги будут вставлены в header страницы',
	header: [
		{
			tagName: 'meta',
			attributes: [
				{
					$value_type: 'textarea',
					attribute: 'name',
					value: 'description'
				},
				{
					$value_type: 'textarea',
					attribute: 'content',
					value: 'Greenside – кальянная в цетре москвы'
				}
			],
			content: ''
		},
		{
			tagName: 'title',
			attributes: [],
			content: 'Greenside'
		}
	]
}