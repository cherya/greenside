{
	$title_type: 'title',
	title: 'Основное',
	phone: '+7 (999) 995-17-17',
	address: 'г.Москва, Наставнический переулок 17А',
	reviewEmail: 'review@greenside.com',
	leftMenu: [
		{
			title: 'Greenside',
			hash: 'main'
		},
		{
			title: 'Немного о нас',
			hash: 'about'
		},
		{
			title: 'Меню',
			hash: 'menu'
		},
		{
			title: 'Фото',
			hash: 'gallery'
		},
		{
			title: 'Скидки и акции',
			hash: 'discount'
		},
		{
			title: 'О нас говорят',
			hash: 'reviews'
		}
	],
	workTime: [
		{
			day: 'ВС — ЧТ',
			time: '12:00 — 02:00'
		},
		{
			day: 'ПТ — СБ',
			time: '14:00 — 04:00'
		}
	],
	vkURL: 'https://vk.com/greenside_hookah',
	facebookURL: 'https://www.facebook.com/greensidehookah/',
	instagramURL: 'https://www.instagram.com/greenside_hookah/',
	instagramName: 'greenside_hookah',
   redirectLink: 'https://www.wikiwand.com/ru/Курение#/Вред_для_здоровья'
}