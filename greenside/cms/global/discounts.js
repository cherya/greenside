{
   description: 'Мы беспокоимся о том, что бы вам было приятно находиться у нас в гостях и для этого мы ввели накопительную систему скидок для наших гостей',
   discounts: [
      {
         $image_type: 'image',
         image: '/assets/img/card1.jpg',
         title: 'Скидка 5%',
         text: 'Действует от общей суммы',
         price: '50 000₽'
      },
      {
         $image_type: 'image',
         image: '/assets/img/card2.jpg',
         title: 'Скидка 10%',
         text: 'Действует от общей суммы',
         price: '100 000₽'
      },
      {
         $image_type: 'image',
         image: '/assets/img/card3.jpg',
         title: 'Скидка 15%',
         text: 'Действует от общей суммы',
         price: '150 000₽'
      }
   ]
}